


// Activity instructions
// 1. Using and argument, parameter, and return keyword, get the value of 'isSuccessful' variable
// 2. Store('verb') the invocation to a variable 'paymentResult' (by storing the invocation you are storing the return statement result to a variable)

let downPaymentPrice = 2000;

function checkPayment(payment){ // >> A parameter is already provided
    // The value from the argument is passed to the parameter and will be evaluated by the conditonal statement below (if and else. The 'else' block will only run if the condition from the 'if' statement is not satisfied / evaluated false)
    isSuccessful = undefined;
    if(payment < downPaymentPrice){
        isSuccessful = false;
        // >> To make the value of 'isSuccessful' usable outside the function, pass it through a return keyword. 
        // >> Create a return statement here...
            
        
    }
    else{
        let change = payment - downPaymentPrice;
        console.log("Your change is "+ change);
        isSuccessful = true;
         // >> To make the value of 'isSuccessful' usable outside the function, pass it through a return keyword. 
        // >> Create a return statement here...
        
    }
}


let downPaymentPrice = 2000;

function checkPayment(payment){ 
    isSuccessful = undefined;
    if(payment < downPaymentPrice){
        isSuccessful = false;
        // return ("You still have " + (payment - downPaymentPrice) + " remaining");
     let balance = Math.abs(payment - downPaymentPrice);
     // let balance1 = Math.abs(balance)
     return "Thank you for paying Son Gokou!\nYou still have " + balance + "$ remaining"
    }
    
    else{
        let change = payment - downPaymentPrice;
        // console.log("Your change is "+ change);
        isSuccessful = true;
        return "Thank you for paying Son Gokou!\nHere's your " + change + "$ change.";
    }
}

let paymentResult = checkPayment(1800);
console.log(paymentResult);






/*
    Documentation:
    // Variables and values initialized inside the function are not accessible outside the function (because it is not within its function scope)
    // console.log(isSuccessful); 
            // This will result to an error
*/
   
// A function will not run if not is not called or invoked.     
// >> Create your invocation, the invocation must be stored to a variable 'paymentResult' with a provided number as an argument
// >> Display the value of 'paymentResult' through console.log()

function compute(operation, num1, num2){
    if(operation == "add" || operation == "sum"){
        return num1 + num2;
    }
    else if(operation == "subtract" || operation == "difference"){
        return num1 - num2;
    }
    else if(operation == "multiply" || operation == "product"){
        return num1 * num2;
    }
    else if(operation == "divide" || operation == "qoutient"){
        return num1 / num2;
    }
    else if(operation == "remainder" || operation == "modulo"){
        return num1 % num2;
    }
    else if(operation == "exponent" || operation == "power"){
        return num1 ** num2;
    }
    else{
        return "Error: Unknown Operation";
    }
}

let value = compute("multiply", 2,4)
console.log(value);


function displayName(name = "Gel"){
    console.log("My name is " +name);
}

displayName();

function getNumber(x,y,z){
    console.log("x");
    return z
}

let number = getNumber(2,5,6);
console.log(number);